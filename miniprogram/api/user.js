
//验证用户是否存在数据库
/**
 * @param {String} username //用户名
 * @param {Number} sex //性别
 * @param {String} headPortrait //头像地址
 */
export const verifyUser = async function (params) {
  let data = {
    username:params.nickName,
    sex:params.gender,
    headPortrait:params.avatarUrl
  }
  const {
    result
  } = await wx.cloud.callFunction({
    name: 'login',
    data:data
  })
  return result.data
}
//更新用户信息

/**
 * @param {String} username //用户名
 * @param {Number} sex //性别
 * @param {String} headPortrait //头像地址
 */
export const updateUser = async function(params){
  wx.showLoading({
    title: '加载中',
  })
  const {result} = await wx.cloud.callFunction({
    name:"user",
    data:{
      api:"update_user",
      data:params
    }
  })
  wx.hideLoading()
  return result.data
}
//更新用户头像
/**
 * @param {String} cloudPath //云地址
 * @param {String} filePath //临时地址
 */
export const updateAvatar = async function(params){
  const {result} = await wx.cloud.callFunction({
    name:"user",
    data:{
      api:"update_user_avatar",
      data:params
    }
  })
  return result.data
}
//获取用户信息
export const getUserInfo = async function(params){
  const {result} = await wx.cloud.callFunction({
    name:"user",
    data:{
      api:"user_info",
      data: params
    }
  })
  return result.data
}
//获取用户的评论
/**
 * @param {String} type //评论类型 dynamic appeal
 * @param {Number} page //当前页
 * @param {number} number //每页数据量
 */
export const getUserComment = async function(params){
  const {result} = await wx.cloud.callFunction({
    name:"user",
    data:{
      api:"user_comment",
      data:params
    }
  })
  return result.data
}