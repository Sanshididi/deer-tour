const app = getApp()
import {
  getUserInfo,
  verifyUser
} from "../../../api/index"

Page({

  /**
   * 页面的初始数据
   */
  data: {
    userInfoImage: "",
    wjUser: {}
  },
  pageData: {
    pageNO: 1,
    pageSize: 10
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.getSetting({
      success(res) {
        if (res.authSetting['scope.userInfo']) {
          // 已经授权，可以直接调用 getUserInfo 获取头像昵称
          wx.getUserInfo({
            success: async function (res) {
              const data = await verifyUser(res.userInfo)
              if (data) {
                wx.setStorageSync('wjUser', data)
                wx.navigateBack({
                  delta: 1
                })
              }
            }
          })
        }
      }
    })

    let wjUser = wx.getStorageSync('wjUser')
    // if (!wjUser) {
    //   wx.navigateTo({
    //     url: '/pages/welcome/welcome',
    //   })
    //   return
    // }
    this.setData({
      wjUser: wjUser,
      userInfoImage:wjUser.headPortrait
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getUserInfoShow()
  },
  // 去编辑页面
  gotoEditProfile() {
    wx.navigateTo({
      url: '/pages/my/editProfile/editProfile',
    })
  },
  gotoUserAppeal: function () {
    wx.navigateTo({
      url: '/pages/my/myAppeal/myAppeal',
    })
  },
  gotoUserDynamic: function () {
    wx.navigateTo({
      url: '/pages/my/mydynamic/mydynamic',
    })
  },
  getUserInfoShow: async function () {
    const result = await getUserInfo({})
    if (result) {
      this.setData({
        wjUser: result
      })
    }
  },
  getUserInfo: async function (e) {
    //获取用户的信息
    const data = await verifyUser(e.detail.userInfo)
    if (data) {
      wx.setStorageSync('wjUser', data)
      wx.navigateBack({
        delta: 1
      })

      this.setData({
        userInfoImage:  e.detail.userInfo.avatarUrl
      })
    }
  },
  gotoUserComment: function () {
    wx.navigateTo({
      url: '/pages/my/myComment/myComment',
    })
  }
})