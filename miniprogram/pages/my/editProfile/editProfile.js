import {
  updateUser,
  updateAvatar,
  uploadSecurityImg
} from "../../../api/index"
const app = getApp();


Page({

  /**
   * 页面的初始数据
   */
  data: {
    region: ['山东省', '青岛市', '崂山区'],
    wjUser: {
      username: "",
      email: ""
    },
    headPortrait: []

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let wjUser = wx.getStorageSync('wjUser')
    this.setData({
      wjUser: wjUser
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  // 选择文件
  uploadHeadPortrait: async function () {
    // 选择图片
    const uploadResult = await uploadSecurityImg({
      count: 1
    })
    console.log(uploadResult)
    if (uploadResult !== 0) {
      wx.showLoading({
        title: '正在上传',
      })
      const cloudPath = uploadResult[0].file.match(/\.[^.]+?$/)[0]
      const result = await updateAvatar({
        cloudPath: cloudPath,
        filePath: uploadResult[0].buffer
      })
      if (result) {
        this.setData({
          [`wjUser.headPortrait`]: result.cloudPath
        })
        wx.setStorageSync('wjUser', this.data.wjUser)
        wx.hideLoading()
      }
    }



  },


  // 修改用户资料
  updateUser: async function (e) {
    let result = await updateUser({
      username: this.data.wjUser.username,
      email: this.data.wjUser.email,
      sex: this.data.wjUser.sex
    })
    if (result) {
      wx.setStorageSync("wjUser", result)
      wx.showToast({
        title: '修改成功'
      })
    }
  },
  userNameChange: function (e) {
    this.setData({
      [`wjUser.username`]: e.detail.value
    })
  },
  emailChange: function (e) {
    //修改邮箱
    this.setData({
      [`wjUser.email`]: e.detail.value
    })
  },
  radioChange: function (e) {
    //性别修改
    this.setData({
      [`wjUser.sex`]: e.detail.value
    })
  },
  RegionChange: function(e) {
    this.setData({
      region: e.detail.value
    })
  }
})