// pages/my/myComment/myComment.js
import {
  pageAppealComment,
  appealPutComment,
  
  getUserComment} from "../../../api/index";
import {timeFormat} from "../../../utils/util";

import util from '../../../utils/util.js';

const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {

    appealId: '',
    comment: '' ,
    CustomBar: app.globalData.CustomBar,
    tabActive:"appeal",
    commentList:[],
    page:1,
    loading:false,
    finishStatus:false,
    refreshStatus:false  //下拉刷新状态
  },
  pageData: {
    pageNO: 1,
    pageSize: 10
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let appealId = options.appealId

    this.setData({
      appealId: appealId
    })

    // this.queryComment();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    this.getUserComment()
   //this.queryComment();
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    // this.queryComment();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },



  tabClick:function(e){
    const type = e.currentTarget.dataset.type
    this.setData({
      tabActive:type,
      commentList:[],
      finishStatus:false
    })
    console.log(type)
    this.getUserComment({type:type,page:1,number:20})
  },
  commentClick:function(e){
    const type = this.data.tabActive
    const id = e.currentTarget.dataset.dynamicid 
    if(type==="dynamic"){
      wx.navigateTo({
        url: '/pages/dynamicDetail/dynamicDetail?dynamicId='+id,
      })
    }
  },
  scrollBottomEvent:function(){
    if(!this.data.finishStatus){
      this.setData({
        page:this.data.page+1,
      })
      this.getUserComment()
    }
  },
  refreshEvent:function(){
    //下拉刷新
    console.log("asdasd")
    this.setData({
      page:1,
      commentList:[],
      finishStatus:false
    })
    this.getUserComment()
  },
  getUserComment: async function(){
    if(this.data.finishStatus){
      return 
    }
    this.setData({
      loading:true
    })
    const result = await getUserComment({type:this.data.tabActive,page:this.data.page,number:20})
    result.data.map(item=>{
      item.createTime = timeFormat(item.createTime)
    })
    this.setData({
      commentList:this.data.commentList.concat(result.data),
      loading:false,
      finishStatus:result.data.length===20?false:true,
      refreshStatus:false
    })
    console.log(result)
  },

  // 查询评论
  queryComment: async function () {

    let data = {
      appealId: this.data.appealId,
      pageNO: this.pageData.pageNO,
      pageSize: this.pageData.pageSize
    }

    let commentList = await pageAppealComment(data);

    commentList.forEach(item => {
      item.createTime = util.format(new Date(item.createTime))
    })

    this.setData({
      commentList: commentList
    })

    console.log(commentList)
  }


})



