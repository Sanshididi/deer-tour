// pages/message/message.js

const app = getApp()
import {
  queryAllChat
} from "../../api/index"
import util from '../../utils/util.js';

Page({

  /**
   * 页面的初始数据
   */
  data: {
    userInfo: {},
    chatList: []
  },


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {


  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

    this.queryAllChat()

  },


  // 去消息 对话页面
  gotoMsgDetail(e) {

    let chatId = e.currentTarget.dataset.chatId

    wx.navigateTo({
      url: '../messageDetail/messageDetail?chatId=' + chatId,
    })

  },

  // 查询用户所有的 聊天室/聊天列表
  queryAllChat: async function (e) {

    let result = await queryAllChat();

    result.forEach((item, index) => {

      item.createTime = util.format(new Date(item.createTime))

    })

    this.setData({
      chatList: result,
    })

    console.log(result)

    wx.stopPullDownRefresh()



  },


  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

    this.queryAllChat()

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }

  , // ListTouch计算方向
  ListTouchMove(e) {
    this.setData({
      ListTouchDirection: e.touches[0].pageX - this.data.ListTouchStart > 0 ? 'right' : 'left'
    })
  },

  // ListTouch计算滚动
  ListTouchEnd(e) {
    if (this.data.ListTouchDirection == 'left') {
      this.setData({
        modalName: e.currentTarget.dataset.target
      })
    } else {
      this.setData({
        modalName: null
      })
    }
    this.setData({
      ListTouchDirection: null
    })
  }
})