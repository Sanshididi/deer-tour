// pages/dynamicPublish/dynamicPublish.js
import {
  addDynamic,
  dynamicTag,
  uploadSecurityImg
} from "../../api/index"
Page({

  /**
   * 页面的初始数据
   */
  data: {
    index: null,
				imgList: [],
    imageArray: [],
    message: "",
    tagList: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

    this.queryDynamicTag();

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },



  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    clearTimeout(this.timer)
  },
  messageChange: function (e) {
    this.setData({
      message: e.detail.value
    })
  },
  uploadImg: async function () {
    let imgArray = this.data.imageArray

    const result = await uploadSecurityImg({
      count: 4
    })
    if (imgArray.length + result.length > 4) {
      return wx.showToast({
        title: '图片太多啦',
        icon: 'none'
      })
    }
    if (result.length !== 0) {
      let uploadArr = result.map(item => item.file)
      this.setData({
        imgList: imgArray.concat(uploadArr)
      })
      console.log(uploadArr)
    }
  },
  submitDynamic: async function () {

    // 诉求标签
    let dynamicTag = []
    this.data.tagList.forEach(item => {
      if (item.check) {
        dynamicTag.push(item)
      }
    })

    let result = await addDynamic({
      images: this.data.imgList,
      message: this.data.message,
      dynamicTag: dynamicTag
    })
    if (result._id) {
      wx.showToast({
        title: '发布成功',
        mask: true,
        success: () => {
          this.timer = setTimeout(() => {
            wx.navigateBack({
              delta: 1
            })
          }, 1000)
        }
      })
    }
    console.log(result)
  },

  // 查询诉求标签
  async queryDynamicTag() {

    let tagList = await dynamicTag()
    this.setData({
      tagList: tagList
    })


  },


  handleTag(e) {

    let index = e.currentTarget.dataset.index

    let tagList = this.data.tagList
    tagList[index].check = tagList[index].check ? false : true

    this.setData({
      tagList: tagList
    })


  },
  // ChooseImage() {
  //   wx.chooseImage({
  //     count: 4, //默认9
  //     sizeType: ['original', 'compressed'], //可以指定是原图还是压缩图，默认二者都有
  //     sourceType: ['album'], //从相册选择
      
  //     success: (res) => {
  //       if (this.data.imgList.length != 0) {

  //         this.setData({
  //           imgList: this.data.imgList.concat(res.tempFilePaths)
  //         })
  //       } else {
  //         this.setData({
  //           imgList: res.tempFilePaths
  //         })
  //       }
  //     }
  //   });
  // },
  ViewImage(e) {
    wx.previewImage({
      urls: this.data.imgList,
      current: e.currentTarget.dataset.url
    });
  },
  DelImg(e) {
    wx.showModal({
      title: '你好',
      content: '确定要删除这段回忆吗？',
      cancelText: '再看看',
      confirmText: '确定',
      success: res => {
        if (res.confirm) {
          this.data.imgList.splice(e.currentTarget.dataset.index, 1);
          this.setData({
            imgList: this.data.imgList
          })
        }
      }
    })
  }

})

