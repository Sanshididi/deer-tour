//index.js
//获取应用实例
const app = getApp()
var utils = require('../../utils/utils.js');

Page({

  /**
   * 页面的初始数据
   */
  data: {
    guess:[]
    ,
    list:[],
    imgList: [
      '/image/ad1.jpg',
      '/image/ad2.jpg',
      '/image/ad3.jpg',
      '/image/ad4.jpg',
      '/image/ad5.jpg'
    ],
    navList: [
      // { icon: '/image/nav-icon/diantai.png', events: 'goToBangDan', text: '榜单' },
      // { icon: '/image/nav-icon/diantai.png', events: 'goToBangDan', text: '听小说' },
      // { icon: '/image/nav-icon/diantai.png', events: 'goToBangDan', text: '情感电台' },
      // { icon: '/image/nav-icon/diantai.png', events: 'goToBangDan', text: '听知识' },

    ],
    swiperCurrent: 0,
  },
  //轮播图改变事件
  swiperChange: function (e) {
    this.setData({
      swiperCurrent: e.detail.current
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

    var that = this
    wx.cloud.callFunction({
      name:"getDetailList",
      data:"",
    }).then(res=>{
      console.log("从数据库那固定数据",res)
      that.setData({
        guess: res.result.data[0].result.data,
        showitem:true,
        list:res.result.data
        // cityNews: res.result.data[0].result.data
      })
    }).catch(res=>{console.log("数据库也没拿到",res)})

  },
  goToBangDan: function () {
    wx.navigateTo({
      url: '/pages/index/bangdan/bangdan',
    })
  },
  gotoDetails(e) {
   
    var url = e.currentTarget.dataset.coverimg;
    var title = e.currentTarget.dataset.title;
    var content=e.currentTarget.dataset.content;
    
    wx.navigateTo({
      url: '/pages/details/details?url=' + url + '&title=' + title+'&content='+content
    })
  }
})
