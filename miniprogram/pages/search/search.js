// pages/search/search.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    hotData:[
      {title:"自然风光",icon:"icon-jiantouUp",color:"text-orange"},
      {title:"名胜古迹",icon:"icon-jiantouUp",color:"text-red"},
      {title:"公园",icon:"icon-jiantouDown",color:"text-green"},
      {title:"海洋馆",icon:"icon-jiantouUp",color:"text-red"},
      {title:"高空景观",icon:"icon-jiantouDown",color:"text-red"},
      {title:"观光街区",icon:"icon-jiantouUp",color:"text-red"},
    ]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})