import {
  getUserInfo,
  pageAppeal,
  appealToEndorse,
  appealToCancel
} from "../../api/index"
import util from '../../utils/util.js';

Page({

  /**
   * 页面的初始数据
   */
  data: {
    openid: null,
    height: null,
    userInfo: {},

    selectTab: [
    ],

    appealList: []
  },

  pageData: {
    pageNO: 1,
    pageSize: 10,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

    this.setData({
      openid: options.openid
    })

    this.queryProperty()
    this.queryAppealList()


  },

  getUserInfo: async function () {
    const result = await getUserInfo({
      openid: this.data.openid
    })
    if (result) {
      this.setData({
        userInfo: result
      })
    }
  },

  checkTab(e) {

    let id = e.currentTarget.dataset.id

    let tabs = this.data.selectTab
    tabs.forEach(item => {
      if (item.id === id) {
        item.check = true
      } else {
        item.check = false
      }
    })

    this.setData({
      selectTab: tabs
    })

  },


  // 查询第三部分的属性
  queryProperty() {

    let that = this

    const query = wx.createSelectorQuery()
    query.select('.function-icon-father').boundingClientRect()
    query.exec(function (res) {
      that.setData({
        height: res[0].bottom + 120
      })
    })

  },


  async queryAppealList() {

    let data = {
      openid: this.data.openid,
      createTime: true,
      nearby: false,
      pageNO: this.pageData.pageNO,
      pageSize: this.pageData.pageSize
    }

    const result = await pageAppeal(data);

    result.forEach(item => {
      item.createTime = util.format(new Date(item.createTime))
    })

    this.setData({
      appealList: result
    })

  },

  // 为诉求点赞
  appealEndorse: async function (e) {

    let appealId = e.currentTarget.dataset.id

    let data = {
      appealId: appealId,
    }

    let result = await appealToEndorse(data);

    if (result._id) {
      let dataList = this.data.appealList
      dataList.forEach((item, index) => {
        if (item._id == appealId) {
          dataList[index].isEndorse = true
          dataList[index].endorseCount = dataList[index].endorseCount + 1
        }
      })
      this.setData({
        appealList: dataList
      })
    } else {

      wx.showToast({
        title: '点赞失败',
        icon: 'none'
      })

    }





  },

  // 取消点赞
  cancelEndorse: async function (e) {

    let appealId = e.currentTarget.dataset.id
    let data = {
      appealId: appealId
    }

    let result = await appealToCancel(data);

    if (result.stats.removed > 0) {

      // 改变值
      let dataList = this.data.appealList
      dataList.forEach((item, index) => {
        if (item._id == appealId) {
          dataList[index].isEndorse = false
          dataList[index].endorseCount = dataList[index].endorseCount - 1
        }
      })
      this.setData({
        appealList: dataList
      })


    } else {

      wx.showToast({
        title: '取消失败',
        icon: 'none'
      })

    }





  },

  // 进入诉求详情
  gotoDetails(e) {

    let id = e.currentTarget.dataset.id

    wx.navigateTo({
      url: '/pages/appealDetails/appealDetails?appealId=' + id
    })

  },



  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

    this.getUserInfo()

  },


  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})