<h1 align="center">交友出行小程序</h1>
<p align="center"><img src="https://img.shields.io/badge/version-1.0-red.svg"></p>

这是个人写的`大学生创新创业参赛作品`的微信小程序;面向学生为学生提供一个可以发布结伴出行信息的平台
<a href="https://github.com/weilanwl/ColorUI" >colorUI</a>


### 技术说明
🔹技术手段：利用前端框架<a href="https://github.com/weilanwl/ColorUI" >colorUI</a>配合html、css、js小程序原生语言开发；
后端使用腾讯<a href="https://developers.weixin.qq.com/miniprogram/dev/wxcloud/basis/getting-started.html" >云开发</a> 利用JSON，云存储， 数据库,云函数api等实现后台数据处理
### TODO
- [√]标准的微信登录
- [√]<a href="https://lbs.qq.com/miniProgram/jsSdk/jsSdkGuide/jsSdkOverview">腾讯地图</a>位置服务
- [√]利用<a href="https://developers.weixin.qq.com/miniprogram/dev/wxcloud/guide/database/realtime.html">实时数据推送</a>实现陌生人聊天 
- [√] 根据<a href="https://developers.weixin.qq.com/community/develop/article/doc/00060e2c9b45183b160b584b251813">经纬度信息计算距离</a>展示附近的人动态
-  [√]配合文字图片发布社交动态、旅游攻略；

- [√]浏览系统推荐本地景点
- [√]支持搜索本地热门景点

- [√]在相关动态点赞评论
- [√]动态可依据点赞量排序
- [√]支持基与关键词，位置搜索动态  

- [√]个人信息管理，查看个人动态，查看留言记录


### 运行

* clone 代码带本地在微信开发者工具中导入项目即可
* <a href="https://www.jianshu.com/p/2d9e226113ee">安装node依赖</a>
* 开启云开发服务
* 部署云函数
* 创建数据库
* **表名严格按照数据库脚本文件名字创建**，数据库的数据结构打包为json文件(DB文件夹目录(数据删除，自行新增测试数据即可))，一键导入就可以了
* 体验地址
<P align="center">
<img src="/git_img/716E5E4D657B5020F2B72B1A39551C47.png" style="zoom:20%"/>
</p>
  
### 项目截图
<P align="center">
<img src="./git_img/QQ截图20210417145645.png"/>
<img src="./git_img/QQ截图20210417145659.png"/>
</p>




###  建议&联系

欢迎您的建议或者是提出Bug

  * 邮箱：1246984534@qq.com 你可以在这里联系我
