// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()

// 云函数入口函数
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext()

  return getNews(event)
}

async function getNews(event) {
  return cloud.database().collection('scenicSpot').get({
    success(res) { console.log("成功", res) },
    fail(error) { console.log("失败", error) }
  })
}
