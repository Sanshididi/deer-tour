// 云函数入口文件
const cloud = require('wx-server-sdk')
cloud.init()
const db = cloud.database()

// 诉求 保存
 const appealSave = async function (params) {
  const wxContext = cloud.getWXContext()

  let data = {
    _openid: wxContext.OPENID,
    title: params.title,
    content: params.content,
    browseCount: 0,
    commentCount: 0,
    endorseCount: 0,
    coordinates: {
      type: 'Point',
      coordinates: [params.longitude || 113, params.latitude || 23]
    },
    appealTag: params.appealTag || [],
    appealMaterial: params.appealMaterial || [],
    createTime: db.serverDate(),
    updateTime: db.serverDate(),
  }


  try {

    let result = db.collection('appeal').add({
      data: data
    })

    return result

  } catch (error) {

    console.log('appealSave error' + error)

    return false
  }

}

module.exports = appealSave