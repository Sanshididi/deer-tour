// 云函数入口文件
const cloud = require('wx-server-sdk')
cloud.init()
const db = cloud.database()

// 发送诉求评论
const appealPutComment = async function (params) {
  const wxContext = cloud.getWXContext()

  let data = {
    _openid: wxContext.OPENID,
    appealId: params.appealId,
    nickName:params.nickName,
    headPortrait:params.headPortrait,
    content: params.content,
    createTime: db.serverDate(),
    updateTime: db.serverDate(),
  }

  try {

    let result = await db.collection('appealComment').add({
      data: data
    })

    return result

  } catch (error) {

    console.log('appealPutComment error' + error)

    return false

  }


}

module.exports = appealPutComment